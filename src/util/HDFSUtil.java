package util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.*;

public class HDFSUtil {
	private FileSystem fs;

	public HDFSUtil(FileSystem fs) {
		this.fs = fs;
	}

	public List<Path> getFiles(String dir) {
		List<Path> files = new ArrayList<Path>();
		Path path = new Path(dir);
		FileStatus[] fileStatus = null;
		try {
			fileStatus = fs.listStatus(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (fileStatus != null) {
			for (FileStatus file : fileStatus) {
				if (!file.isDir()) {
					files.add(file.getPath());
				}
			}
		}
		return files;
	}

	public Path rename(Path src, String newName) {
		Path dest = new Path(src.getParent().getName() + "/" + newName);
		try {
			fs.rename(src, dest);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dest;
	}

	public void copyToLocal(Path src, String localPath) {
		try {
			fs.copyToLocalFile(src, new Path(localPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean move(Path src, String destPath) {
		boolean result = false;
		try {
			result = fs.rename(src, new Path(destPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean delete(String path) {
		boolean result = false;
		try {
			result = fs.delete(new Path(path), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean delete(Path path) {
		boolean result = false;
		try {
			return fs.delete(path, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void close() {
		try {
			fs.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
