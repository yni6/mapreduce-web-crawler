package util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LocalUtil {
	public static Properties readProperties(String configFile) throws IOException {
		Properties p = new Properties();
		InputStream in = new BufferedInputStream(new FileInputStream(new File(configFile)));
		p.load(in);
		in.close();

		return p;
	}
	
	public static Properties parseArgs(String[] args) {
		Properties p = new Properties();
		for (int i = 0; i < args.length; i++) {
			if ("--config".equals(args[i])) {
				p.setProperty("configFile", args[i + 1]);
			}
		}
		return p;
	}
	
	public static void sleep(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
