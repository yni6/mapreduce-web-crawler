package crawl;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import model.UrlTuple;
import model.UrlTuple.UrlStatus;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import config.Config;
import util.HDFSUtil;
import util.LocalUtil;

public class UrlCrawl {
	private static long ver = 0;
	private static long jobCount = 0;

	public static class UrlMapper extends Mapper<Object, Text, Text, Text> {
		private UrlTuple seed = new UrlTuple();

		@Override
		public void map(Object key, Text value, Context context) {
			String line = value.toString();
			if (!UrlTuple.validate(line)) {
				return;
			}
			String[] pair = line.split(UrlTuple.KV_DELIMITER);
			seed.setUrl(pair[0]);
			String[] attrs = pair[1].split(UrlTuple.ATTR_DELIMITER);
			seed.setCount(Integer.parseInt(attrs[0]));
			Document doc = null;
			try {
				doc = Jsoup.connect(seed.getUrl()).get();
			} catch (IOException e) {
				System.out.println("Jsoup failed to connect " + seed.getUrl());
				seed.setStatus(UrlStatus.CRAWLED_FAILED);
				// record seed as crawled and failed
				try {
					context.write(new Text(seed.getUrl()), new Text(seed.getAttributes()));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			if (doc == null) {
				return;
			}
			Elements links = doc.select("a[href]");
			for (Element ele : links) {
				UrlTuple ut = new UrlTuple(ele.attr("abs:href").trim().split(" ")[0].split("\t")[0]);
				ut.setCount(1);
				ut.setStatus(UrlStatus.NOT_CRAWLED);
				// record newly found url
				try {
					context.write(new Text(ut.getUrl()), new Text(ut.getAttributes()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			seed.setStatus(UrlStatus.CRAWLED_SUCCESS);
			// record seed as crawled successfully
			try {
				context.write(new Text(seed.getUrl()), new Text(seed.getAttributes()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static class UrlReducer extends Reducer<Text, Text, Text, Text> {
		private UrlTuple ut = new UrlTuple();

		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) {
			ut.setUrl(key.toString());
			ut.setStatus(0);
			int sum = 0;
			for (Text value : values) {
				String[] attrs = value.toString().split(UrlTuple.ATTR_DELIMITER);
				int count = Integer.parseInt(attrs[0]);
				sum += count;
				int status = Integer.parseInt(attrs[1]);
				if (ut.getStatus() < status) {
					ut.setStatus(status);
				}
			}
			ut.setCount(sum);

			System.out.println("Newly found URL: " + ut.getUrl());
			try {
				context.write(new Text(ut.getUrl()), new Text(ut.getAttributes()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void urlCrawlDriver(Config config) throws Exception {
		Configuration conf = new Configuration();
		HDFSUtil hdfs = new HDFSUtil(FileSystem.get(conf));
		String inputDir = config.getSeedDir();

		while (true) {
			// check urls candidate seeds dir to get new urls seeds
			List<Path> seeds = hdfs.getFiles(config.getCandidateDir());
			if (seeds.isEmpty()) {
				LocalUtil.sleep(3);
				continue;
			}

			// clean input dir
			List<Path> oldSeeds = hdfs.getFiles(inputDir);
			for (Path oldSeed : oldSeeds) {
				hdfs.delete(oldSeed);
				System.out.println("Deleted " + oldSeed.getName());
			}

			// move urls seeds to input dir
			for (Path seed : seeds) {
				hdfs.move(seed, inputDir);
				System.out.println("Moved " + seed.getName() + " from " + config.getCandidateDir() + " to " + inputDir);
			}

			// start a crawling job
			String jobName = config.getSeedDir() + "-" + String.valueOf(++ver);
			Job job = new Job(conf, jobName);
			job.setJarByClass(UrlCrawl.class);
			job.setMapperClass(UrlMapper.class);
			job.setCombinerClass(UrlReducer.class);
			job.setReducerClass(UrlReducer.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(Text.class);

			String outputDir = job.getJobName();
			String[] ioArgs = { inputDir, outputDir };
			String[] otherArgs = new GenericOptionsParser(conf, ioArgs).getRemainingArgs();
			FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
			FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
			job.waitForCompletion(config.isLogVerbose());

			// move output files to cache dir
			List<Path> outputs = hdfs.getFiles(outputDir);
			for (Path output : outputs) {
				if (output.getName().contains("part-r")) {
					Path newOutput = hdfs.rename(output, "cache-" + String.valueOf(System.currentTimeMillis()));
					hdfs.move(newOutput, config.getCacheDir());
					System.out.println("Moved " + newOutput.getName() + " from " + outputDir + " to " + config.getCacheDir());
				}
			}

			// delete output dir
			hdfs.delete(outputDir);
			System.out.println("Deleted " + outputDir);

			final int maxNumJobs = config.getMaxNumJobs();
			if (maxNumJobs != -1 && ++jobCount >= maxNumJobs) {
				break;
			}
		}

		hdfs.close();
	}

	private static void driver(Config config, int numExps) {
		if (numExps > 3) {
			return;
		}
		try {
			urlCrawlDriver(config);
		} catch (Exception e) {
			e.printStackTrace();
			driver(config, ++numExps);
		}
	}

	public static void main(String[] args) throws IOException {
		Properties p = LocalUtil.parseArgs(args);
		Config config = Config.getInstance(p.getProperty("configFile"));

		driver(config, 0);
	}
}
