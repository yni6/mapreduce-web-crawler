package refine;

import java.util.List;
import java.util.Properties;

import model.UrlTuple;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.io.*;

import config.Config;
import util.HDFSUtil;
import util.LocalUtil;

public class UrlRefine {
	private static long ver = 0;
	private static long jobCount = 0;

	public static class UrlMapper extends Mapper<Object, Text, Text, Text> {

		@Override
		public void map(Object key, Text value, Context context) {
			String line = value.toString();
			if (!UrlTuple.validate(line)) {
				return;
			}
			String[] pair = line.split(UrlTuple.KV_DELIMITER);
			try {
				context.write(new Text(pair[0]), new Text(pair[1]));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static class UrlReducer extends Reducer<Text, Text, Text, Text> {
		private UrlTuple ut = new UrlTuple();

		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) {
			ut.setUrl(key.toString());
			ut.setStatus(0);
			int sum = 0;
			for (Text value : values) {
				String[] attrs = value.toString().split(UrlTuple.ATTR_DELIMITER);
				int count = Integer.parseInt(attrs[0]);
				sum += count;
				int status = Integer.parseInt(attrs[1]);
				if (ut.getStatus() < status) {
					ut.setStatus(status);
				}
			}
			ut.setCount(sum);
			try {
				context.write(new Text(ut.getUrl()), new Text(ut.getAttributes()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void urlRefineDriver(Config config) throws Exception {
		Configuration conf = new Configuration();
		HDFSUtil hdfs = new HDFSUtil(FileSystem.get(conf));
		String inputDir = config.getUrldbDir();

		while (true) {
			// check cache dir to get new urls files
			List<Path> resources = hdfs.getFiles(config.getCacheDir());
			if (resources.isEmpty()) {
				LocalUtil.sleep(3);
				continue;
			}

			// move new urls files to input dir
			for (Path resource : resources) {
				hdfs.move(resource, inputDir);
				System.out.println("Moved " + resource.getName() + " from " + config.getCacheDir() + " to " + inputDir);
			}

			// start a dedup job
			String jobName = config.getUrldbDir() + "-" + String.valueOf(++ver);
			Job job = new Job(conf, jobName);
			job.setJarByClass(UrlRefine.class);
			job.setMapperClass(UrlMapper.class);
			job.setCombinerClass(UrlReducer.class);
			job.setReducerClass(UrlReducer.class);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(Text.class);
			String outputDir = job.getJobName();
			String[] ioArgs = { inputDir, outputDir };
			String[] otherArgs = new GenericOptionsParser(conf, ioArgs).getRemainingArgs();
			FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
			FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
			job.waitForCompletion(config.isLogVerbose());

			// delete useless files and copy output files to local file system
			List<Path> outputs = hdfs.getFiles(outputDir);
			for (Path output : outputs) {
				if (output.getName().contains("part-r")) {
					Path newOutput = hdfs.rename(output, "urls-" + String.valueOf(System.currentTimeMillis()));
					hdfs.copyToLocal(newOutput, config.getLocalUrldbDir());
					System.out.println("Copied " + newOutput.getName() + " from " + outputDir + " to " + config.getLocalUrldbDir());
				} else {
					hdfs.delete(output);
				}
			}

			// set output dir as next run's input dir and delete old input dir
			String oldInputDir = inputDir;
			inputDir = outputDir;
			hdfs.delete(oldInputDir);
			System.out.println("Dleted " + oldInputDir);

			final int maxNumJobs = config.getMaxNumJobs();
			if (maxNumJobs != -1 && ++jobCount >= maxNumJobs) {
				break;
			}
		}

		hdfs.close();
	}

	private static void driver(Config config, int numExps) {
		if (numExps > 3) {
			return;
		}
		try {
			urlRefineDriver(config);
		} catch (Exception e) {
			e.printStackTrace();
			driver(config, ++numExps);
		}
	}

	public static void main(String[] args) throws Exception {
		Properties p = LocalUtil.parseArgs(args);
		Config config = Config.getInstance(p.getProperty("configFile"));

		driver(config, 0);
	}
}
