package config;

import java.io.IOException;
import java.util.Properties;

import util.LocalUtil;

public class Config {
	private static Config instance;
	private Properties props;
	private int maxNumJobs = -1;
	private String seedDir = "seeds";
	private String candidateDir = "seeds-q";
	private String cacheDir = "cache";
	private String urldbDir = "urldb";
	private String localUrldbDir = "/tmp";
	private boolean logVerbose = true;

	private Config(String configFile) throws IOException {
		props = LocalUtil.readProperties(configFile);
		maxNumJobs = Integer.parseInt(props.getProperty("maxNumJobs"));
		seedDir = props.getProperty("seedDir");
		candidateDir = props.getProperty("candidateDir");
		cacheDir = props.getProperty("cacheDir");
		urldbDir = props.getProperty("urldbDir");
		localUrldbDir = props.getProperty("localUrldbDir");
		logVerbose = Boolean.parseBoolean(props.getProperty("logVerbose"));
	}

	public static synchronized Config getInstance(String configFile) throws IOException {
		if (instance == null) {
			instance = new Config(configFile);
		}
		return instance;
	}

	public int getMaxNumJobs() {
		return maxNumJobs;
	}

	public void setMaxNumJobs(int maxNumJobs) {
		this.maxNumJobs = maxNumJobs;
	}

	public String getSeedDir() {
		return seedDir;
	}

	public void setSeedDir(String seedDir) {
		this.seedDir = seedDir;
	}

	public String getCandidateDir() {
		return candidateDir;
	}

	public void setCandidateDir(String candidateDir) {
		this.candidateDir = candidateDir;
	}

	public String getCacheDir() {
		return cacheDir;
	}

	public void setCacheDir(String cacheDir) {
		this.cacheDir = cacheDir;
	}

	public String getUrldbDir() {
		return urldbDir;
	}

	public void setUrldbDir(String urldbDir) {
		this.urldbDir = urldbDir;
	}

	public String getLocalUrldbDir() {
		return localUrldbDir;
	}

	public void setLocalUrldbDir(String localUrldbDir) {
		this.localUrldbDir = localUrldbDir;
	}

	public boolean isLogVerbose() {
		return logVerbose;
	}

	public void setLogVerbose(boolean logVerbose) {
		this.logVerbose = logVerbose;
	}
}
