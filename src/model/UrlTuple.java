package model;

public class UrlTuple {
	public static enum UrlStatus {
		NOT_CRAWLED, CRAWLED_FAILED, CRAWLED_SUCCESS;

		public static UrlStatus valueOf(int status) {
			UrlStatus us = NOT_CRAWLED;

			return us;
		}
	}

	public static final String KV_DELIMITER = "\t";
	public static final String ATTR_DELIMITER = " ";

	// key
	private String url;
	// attributes
	private int count;
	private UrlStatus status;

	public UrlTuple(String url) {
		this.url = url;
	}

	public UrlTuple() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAttributes() {
		return String.valueOf(count) + ATTR_DELIMITER + String.valueOf(status.ordinal());
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getStatus() {
		return this.status.ordinal();
	}

	public void setStatus(UrlStatus status) {
		this.status = status;
	}

	public void setStatus(int status) {
		switch (status) {
		case 0:
			this.status = UrlStatus.NOT_CRAWLED;
			break;
		case 1:
			this.status = UrlStatus.CRAWLED_FAILED;
			break;
		case 2:
			this.status = UrlStatus.CRAWLED_SUCCESS;
			break;
		default:
			this.status = UrlStatus.NOT_CRAWLED;
			break;
		}
	}

	public static boolean validate(String line) {
		String[] pair = line.split(UrlTuple.KV_DELIMITER);
		if (pair.length != 2 || pair[0].trim().isEmpty()) {
			return false;
		}
		String[] attrs = pair[1].split(UrlTuple.ATTR_DELIMITER);
		if (attrs.length != 2) {
			return false;
		}
		int count = -1;
		int status = -1;
		try {
			count = Integer.parseInt(attrs[0]);
			status = Integer.parseInt(attrs[1]);
		} catch (Exception e) {
			return false;
		}
		if (count <= 0 || status < 0 || status > 2) {
			return false;
		}
		return true;
	}

}
