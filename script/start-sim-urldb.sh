#!/bin/bash

bin=`dirname $0`
bin=`cd $bin; pwd`

configFile=$bin/../conf/config.properties
urldbDir=`grep "localUrldbDir" $configFile | cut -d '=' -f 2`
urldb=$urldbDir/urldb
urlsFiles=$urldbDir/urls*
hiddenFiles=$urldbDir/.urls*
seed=$urldbDir/seed
candidateDir=`grep "candidateDir" $configFile | cut -d '=' -f 2`
hadoop=`env | grep HADOOP_HOME | cut -d '=' -f 2`/bin/hadoop
maxNumJobs=`grep "maxNumJobs" $configFile | cut -d '=' -f 2`
jobCount=0
numUrlsPerSeedFile=20

seedFile=$seed-`date +%N`
if [ -f $urldb ]; then
 awk '$3==0 {print}' $urldb | head -n $numUrlsPerSeedFile > $seedFile
 if [ -s $seedFile ]; then
  echo "Uploading $seedFile to HDFS $candidateDir."
  $hadoop fs -put $seedFile $candidateDir
 fi
 rm -f $seedFile
fi

while true
do
 seedFile=$seed-`date +%N`
 if [ ! -f $urlsFiles ]; then
  sleep 3
  continue;
 fi

 echo "Sorting and merging $urlsFiles."
 sort -r -k2 -n $urlsFiles > $urldb
 echo "Generating $seedFile."
 awk '$3==0 {print}' $urldb | head -n $numUrlsPerSeedFile > $seedFile
 if [ -s $seedFile ]; then
  echo "Uploading $seedFile to HDFS $candidateDir."
  $hadoop fs -put $seedFile $candidateDir
 fi
 echo "Cleaning up."
 rm -f $seedFile $urlsFiles $hiddenFiles

 let "jobCount += 1"
 if [ $maxNumJobs != -1 ] && [ $jobCount -ge $maxNumJobs ]; then
  break;
 fi

 sleep 1
done

exit 0
