#!/bin/bash

bin=`dirname $0`
bin=`cd $bin; pwd`
logDir=$bin/../logs

$bin/start-url-mapred.sh
sleep 3
$bin/start-sim-urldb.sh > $logDir/sim-urldb.log &

exit 0
