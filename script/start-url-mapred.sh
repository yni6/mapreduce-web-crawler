#!/bin/bash

bin=`dirname $0`
bin=`cd $bin; pwd`

configFile=$bin/../conf/config.properties
hadoop=`env | grep HADOOP_HOME | cut -d '=' -f 2`/bin/hadoop
urldbDir=`grep "urldbDir" $configFile | cut -d '=' -f 2`
localUrldb=`grep "localUrldbDir" $configFile | cut -d '=' -f 2`/urldb
seedDir=`grep "seedDir" ${configFile} | cut -d '=' -f 2`
candidateDir=`grep "candidateDir" ${configFile} | cut -d '=' -f 2`
cacheDir=`grep "cacheDir" ${configFile} | cut -d '=' -f 2`
logDir=$bin/../logs

echo "Cleaning up."
$hadoop fs -rmr $candidateDir $cacheDir $seedDir* $urldbDir*
echo "Initializing $seedDir $candidateDir $cacheDir $urldbDir on HDFS."
$hadoop fs -mkdir $seedDir $candidateDir $cacheDir $urldbDir

if [ -f $localUrldb ]; then
 echo "Uploading $localUrldb to HDFS $urldbDir."
 $hadoop fs -put $localUrldb $urldbDir
fi
$hadoop jar $bin/../url-crawl.jar --config $configFile 1>&0 2>$logDir/mapred-crawl.log &
$hadoop jar $bin/../url-refine.jar --config $configFile 1>&0 2>$logDir/mapred-refine.log &

exit 0
